import json
import base64

import paho.mqtt.client as mqtt


MQTT_SERVER = "127.0.0.1"
MQTT_PATH = "image"


def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {str(rc)}")

    client.subscribe(MQTT_PATH)


def on_message(client, userdata, msg):
    mess = json.loads(msg.payload)
    name = mess['name']
    with open(name, "wb") as f:
        f.write(base64.decodebytes(mess['data'].encode('utf-8')))
    print(f"Image {name} Received")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(MQTT_SERVER, 1883, 60)


client.loop_forever()
