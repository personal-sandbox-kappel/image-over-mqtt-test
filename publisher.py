import base64
import json

import paho.mqtt.publish as publish


MQTT_SERVER = "127.0.0.1"
MQTT_PATH = "image"
NAME = "simple.jpeg"

with open(f"/home/kappel/Pictures/{NAME}", "rb") as f:
    file_content = f.read()

data = {
    "name": NAME,
    "data": base64.encodebytes(file_content).decode('utf-8')
}


publish.single(MQTT_PATH, json.dumps(data), hostname=MQTT_SERVER)
